// record.js
import regeneratorRuntime from '../../lib/runtime/runtime';
import { getDateDiff, average } from "../../utils/dateFunc.js";

Page({

    /**
     * 页面的初始数据
     */
    data: {
        dateList: [],
        isInPeriod: true,
        today: '',
        year: 0,
        month: 0,
        totalDay: 0,
        periodCount: 0,
        nextPeriodCount: 0,
        avgPeriodTime: 29,
        periodIntervalList: [],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onShow() {
        const dateList = wx.getStorageSync("dateList") || [];
        const isInPeriod = dateList.length % 2 - 1;
        // console.log(isInPeriod)
        this.getCurrentDate();
        this.getPeriodCount();

        this.setData({ isInPeriod, dateList })
    },

    //绑定点击事件，判断输入的时间是否合理
    bindDateChange(e) {
        // console.log('picker发送选择改变，携带值为', e.detail.value)
        // this.getCurrentDate();

        let dateList = wx.getStorageSync("dateList") || [];
        let val = e.detail.value
        console.log(dateList.length)
        if (dateList.length % 2 === 0) {
            dateList.unshift(e.detail.value);
        } else {
            const start = dateList[0];
            let diff = getDateDiff(start, val);
            console.log(diff)
            if (diff < 8 && diff > 0) {
                dateList.unshift(e.detail.value);
                dateList.sort().reverse()
            } else {
                wx.showToast({
                    title: '该记录不符合规则，历史记录请到"我的"页面查看',
                    icon: 'none',
                    duration: 2000
                })
            }
        }


        wx.setStorageSync("dateList", dateList);
        this.setData({ dateList })
        this.onShow();
    },

    //得到当前时间相关的数据
    getCurrentDate() {
        let now = new Date();
        let year = now.getFullYear();
        // year = year >= 10 ? year : '0' + year;
        let month = now.getMonth() + 1;
        month = month >= 10 ? month : '0' + month;
        let date = now.getDate();
        date = date >= 10 ? date : '0' + date;
        //totalDay 表示该月有多少天
        let totalDay = new Date(year, month, 0).getDate();
        let today = year + '-' + month + '-' + date
        this.setData({
            today,
            year,
            month,
            totalDay,
        })
    },

    //新的记录产生时，需要刷新平均周期时间
    getPeriodCount() {
        this.getAvgPeriodTime();
        const dateList = wx.getStorageSync("dateList") || [];
        let avgPeriodTime = wx.getStorageSync("avgPeriodTime") || 29;
        let today = this.data.today;
        let nextPeriodCount, periodCount = 0;
        if (dateList.length === 0) {
            nextPeriodCount = avgPeriodTime;
        } else if (dateList.length % 2 === 0) {
            nextPeriodCount = avgPeriodTime - getDateDiff(dateList[1], today);
            this.setData({ nextPeriodCount })
        } else if (dateList.length % 2 !== 0) {
            periodCount = getDateDiff(dateList[0], today) + 1;
            this.setData({ periodCount })
        }

    },

    //新的记录产生时，需要刷新平均周期时间
    getAvgPeriodTime() {
        const list = wx.getStorageSync("dateList") || [];
        const length = list.length;

        if (length != 0 && length % 2 === 0) {
            for (var i = 0; i < length; i += 2) {
                let start = list[i + 1];
                // 如果没有前一个记录，则将preStart设置成和start一样，这样使得间隔时间为0，后面0将不显示
                let preStart = list[i + 3] || list[i + 1];
                let dateDiff2 = length >= 3 ? getDateDiff(preStart, start) : 0;
                let periodIntervalList = this.data.periodIntervalList;
                // 间隔期间太短或者太长都应该舍去，不属于正常范围的月经周期
                if (dateDiff2 >= 25 && dateDiff2 <= 40) { periodIntervalList.push(dateDiff2) }
                this.setData({ periodIntervalList })
            }
            let avgPeriodTime = parseInt(average(this.data.periodIntervalList)) || 29;
            this.setData({ avgPeriodTime })
            console.log(avgPeriodTime)
            wx.setStorageSync("avgPeriodTime", avgPeriodTime)
        }
    },

})